require 'redd'
require 'rgl/adjacency'
require 'rgl/dot'
require 'json'
#
# Grabs all Runs that have been, or will be done, and for each one returns an array of Players who have been selected to run
#
# @return [Array<String, Array<String>>] Reddit Names of people who are running together
#
def grab_runs()
    # First thing first: We need to generate our client
    keys=JSON.parse(File.read('./secrets.json'))
    client = Redd.it(client_id: keys['client_id'], secret: keys['secret'])
    runners = client.subreddit('runnerhub').search('flair_name:"Positions Filled"').to_ary.map do |post|#Grabbing all posts that have a run that has been filled
        # Welp we are going to be lazy and abuse hub rules rn soooo ...
        #This horifying bit of code searches the comments on the post for all comments that are not empty, and grabs the author's name
        boop = post.comments.to_ary.select{|c| !c.replies.empty?}.map{|comment| comment.author.name}
        [post.author.name, boop]
    end
    runners
end
if $0==__FILE__
    graph = RGL::DirectedAdjacencyGraph.new
    run_ary=grab_runs()#Our Array of Runners
    # Ok So first of all let's go ahead and Grab our nodes...
    run_ary.each do |run|
        gm = run[0]
        run[1].each do |player|
            graph.add_edge gm, player
        end
    end
    ## Ok we should get weights ...
    run_ary.map{|c| c[0]}.each do |gm|
        # Let's go ahead and grab all of the arrays that the gm has run
        gm_player_array=run_ary.select{|c| c[0]==gm}.map{|a| a[1]}.flatten
        # And Create a Hash that ...
        gm_player_counts=Hash.new(0)
        # We now have this Array ... let's go ahead and count it out
        gm_player_array.each { |player| gm_player_counts[player]+=1}
        # And welp ... Let's go ahead and Weight our graph!
    end
    # puts graph.edges
    puts graph.print_dotted_on
end